package ru.t1.dzelenin.tm.command.user;

import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    private final String NAME = "change-user-password";

    private final String DESCRIPTION = "change password of current user.";

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, password);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
