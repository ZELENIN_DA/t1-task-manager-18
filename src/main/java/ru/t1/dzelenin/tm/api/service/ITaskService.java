package ru.t1.dzelenin.tm.api.service;

import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.enumerated.Sort;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    Task changeTaskStatusIndex(Integer index, Status status);

    Task changeTaskStatusId(String id, Status status);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    List<Task> findAllByProjectId(String projectId);
}
