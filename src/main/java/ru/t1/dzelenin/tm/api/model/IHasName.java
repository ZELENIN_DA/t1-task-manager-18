package ru.t1.dzelenin.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String created);

}
